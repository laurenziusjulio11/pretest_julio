﻿using Dapper;
using PretestCoreJulio.Model;

namespace PretestCoreJulio.Repository.Interface
{
    public interface ICompany
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getCompanyList<T>();
    }
}


