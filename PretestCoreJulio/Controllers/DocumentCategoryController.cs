﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PretestCoreJulio.Repository.Interface;
using PretestCoreJulio.Model;
using System.Data;


namespace PretestCoreJulio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentCategoryController : Controller
    {
        private readonly IDocumentCategory _authentication;
        public DocumentCategoryController(IDocumentCategory authentication)
        {
            _authentication = authentication;
        }

        [HttpGet("DocumentCategoryList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getDocumentCategory()
        {
            var result = _authentication.getDocumentCategoryList<ModelDocumentCategory>();

            return Ok(result);
        }

        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult CreateCategory([System.Web.Http.FromBody] ModelDocumentCategory category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", category.Name, DbType.String);
            dp_param.Add("CreatedBy", category.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocumentCategory>("sp_CreateDocumentCategory", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateCategory([System.Web.Http.FromBody] ModelDocumentCategory category, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", category.Name, DbType.String);
            dp_param.Add("CreatedBy", category.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocumentCategory>("sp_UpdateDocumentCategory", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocumentCategory>("sp_DeleteDocumentCategory", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }

    }
}
