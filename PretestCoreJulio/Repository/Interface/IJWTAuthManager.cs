﻿using Dapper;
using PretestCoreJulio.Model;


namespace PretestCoreJulio.Repository.Interface
{
    public interface IJWTAuthManager
    {
        Response<string> GenerateJWT(ModelUser user);
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getUserList<T>();
    }
}
